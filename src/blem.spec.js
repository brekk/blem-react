/* global jest, test, expect */
/* eslint-disable react/prop-types */
import React from "react"
import * as renderer from "react-test-renderer"
import { withBlem, BlemProvider } from "./with-blem"

console.error = jest.fn()

test(`withBlem`, () => {
  const ElementComponent = ({ className: c }) => (
    <div className={c}>this is an element</div>
  )
  const ElementWithModComponent = ({ className: c }) => (
    <div className={c}>this is an element with a modifier</div>
  )
  const E = () => <strong>test</strong>
  const ElementComponentWithBlem = withBlem(ElementComponent)
  const ModElementWithBlem = withBlem(ElementWithModComponent)
  const E2 = withBlem(E)
  expect(
    renderer
      .create(
        <BlemProvider base="MyComponent">
          <ElementComponentWithBlem bem="element" />
          <ModElementWithBlem bem={[`element2`, `modifier`]} />
          <E2 />
        </BlemProvider>
      )
      .toJSON()
  ).toMatchSnapshot()
})

test(`withBlem, no base`, () => {
  expect(() => renderer.create(<BlemProvider />)).toThrow(
    `[BlemProvider] Expected props.base to be defined on the BlemProvider instance.`
  )
})
test(`withBlem, base is not a string`, () => {
  expect(() => renderer.create(<BlemProvider base={100} />)).toThrow(
    `[BlemProvider] Expected props.base to be a string.`
  )
})

test(`withBlem, with refs`, () => {
  const fillIt = a => {
    return withBlem(props => <strong className={props.className}>{a}</strong>)
  }
  const X = fillIt(`X`)
  const Y = fillIt(`Y`)
  const Z = fillIt(`Z`)
  class MyComp extends React.Component {
    render() {
      return (
        <BlemProvider base="Component">
          <div>
            {[X, Y, Z].map((A, i) => (
              <A bem={[`child`, `` + i]} ref={i} />
            ))}
          </div>
        </BlemProvider>
      )
    }
  }
  expect(
    renderer
      .create(
        <BlemProvider base="MyComponent">
          <MyComp />
        </BlemProvider>
      )
      .toJSON()
  ).toMatchSnapshot()
})
