import React from "react"
import hoist from "hoist-non-react-statics"
import blem from "blem"
import PropTypes from "prop-types"
import { BlemContext } from "./blem-context"

const isProd = () => process.env.NODE_ENV === `production`

export const BlemProvider = props => (
  <BlemContext.Consumer>
    {() => {
      if (!isProd() && !props.base) {
        throw new Error(
          `[BlemProvider] Expected props.base to be defined on the BlemProvider instance.`
        )
      }
      if (!isProd() && typeof props.base !== `string`) {
        throw new TypeError(
          `[BlemProvider] Expected props.base to be a string.`
        )
      }
      const value = blem(props.base)
      return (
        <BlemContext.Provider value={value}>
          {props.children}
        </BlemContext.Provider>
      )
    }}
  </BlemContext.Consumer>
)

BlemProvider.propTypes = {
  base: PropTypes.oneOf([PropTypes.string, PropTypes.array]),
  children: PropTypes.node
}

export const withBlem = Component => {
  const Blemmed = React.forwardRef((props, ref) => (
    <BlemContext.Consumer>
      {blemMaker => (
        <Component
          className={blemMaker.apply(
            null,
            typeof props.bem === `string` ? [props.bem] : props.bem
          )}
          ref={ref}
          {...props}
        />
      )}
    </BlemContext.Consumer>
  ))
  Blemmed.propTypes = {
    bem: PropTypes.oneOf([PropTypes.string, PropTypes.array])
  }
  const name = Component.name | Component.displayName | `Component`
  Blemmed.displayName = `WithBlem(${name})`
  return hoist(Blemmed, Component)
}
