```
██████╗ ██╗     ███████╗███╗   ███╗      ██████╗ ███████╗ █████╗  ██████╗████████╗
██╔══██╗██║     ██╔════╝████╗ ████║      ██╔══██╗██╔════╝██╔══██╗██╔════╝╚══██╔══╝
██████╔╝██║     █████╗  ██╔████╔██║█████╗██████╔╝█████╗  ███████║██║        ██║   
██╔══██╗██║     ██╔══╝  ██║╚██╔╝██║╚════╝██╔══██╗██╔══╝  ██╔══██║██║        ██║   
██████╔╝███████╗███████╗██║ ╚═╝ ██║      ██║  ██║███████╗██║  ██║╚██████╗   ██║   
╚═════╝ ╚══════╝╚══════╝╚═╝     ╚═╝      ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝ ╚═════╝   ╚═╝   
```
# blem-react
> Use [blem](https://www.npmjs.com/package/blem) with [react](https://reactjs.org/)!

## Example usage

```js
import {BlemProvider, withBlem} from 'blem-react'

// wrap your component with a withBlem decorator
const MyComponent = withBlem(
  // and use the className that is provided
  (className: c) => (<div className={c}>Cool</div>)
)
// pass in the "base" prop to the BlemProvider
const App = () => (
  <div>
    <BlemProvider base="Base">
      <MyComponent bem="element"/>
      <MyComponent bem={["element", "modifier"]} />
    </BlemProvider>
  </div>
)
```

The above component would render classnames like so:

```html
<div>
  <div class="Base__element">Cool</div>
  <div class="Base__element--modifier">Cool</div>
</div>
```

See [the tests](https://gitlab.com/brekk/blem-react/blob/master/src/blem.spec.js) and [the snapshots](https://gitlab.com/brekk/blem-react/blob/master/src/__snapshots__/blem.spec.js.snap) for more!

## Source

https://gitlab.com/brekk/blem-react

This is a work-in-progress module, please feel free to file issues on Gitlab.
